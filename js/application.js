$(document).ready(function() {
    var myDataRefRoot = new Firebase('https://certificado.firebaseio.com/');
    var myDataRefCertficados = new Firebase('https://certificado.firebaseio.com/certificados');


    var search_function = function(e) {
        if (e.keyCode == 13 || e.type == "submit") {
            var cedula = $("#search_input").val();
            var result;
            var cursos = 
            '<table class="display table table-striped table-hover table-condensed">'+
              '<thead>'+
                '<tr>'+
                  '<th>Codigo</th>'+
                  '<th>Nombre</th>'+
                  '<th>Fecha Realización</th>'+
                  '<th>Fecha Vencimiento</th>'+
                '</tr>'+
              '</thead>'+
              '<tbody>';
            var curso = "";
            var nombres;
            var apellidos;

            myDataRefCertficados.orderByChild('cedula').equalTo(cedula)
                .once('value', function(snap) {
                    if (snap.val() !== null) {
                        result = snap.val();
                        $.each(result, function(i, val) {
                            nombres = val.nombres;
                            apellidos = val.apellidos;
                            curso =
                                "<tr>" +
                                 
                                  "<td>" + val.codigo_certificado + "</td>" +
                                   "<td>" + val.certificado + "</td>" +
                                  "<td>" + val.fecha_ini + "</td>" +
                                  "<td>" + val.fecha_fin + "</td>" +
                                "</tr>";
                            cursos = cursos + curso ;
                        });

                    } 
                    if (typeof result !== "undefined") {

                        cursos = cursos + "</tbody></table>";
                        $(".dialog_text").html(
                            "<div class='panel panel-info'>"+
                              "<div class='panel-heading'>Validez de Cursos</div>"+
                                "<div class='panel-body'>"+
                                  "<p>El Estudiante:"+
                                    "<mark class='text-capitalize'>" + nombres + " " + apellidos + "</mark>" +
                                    "con cedula: <mark>" + cedula + "</mark>. Ha realizado los siguiente cursos:"+ 
                                  "</p>"+
                                "</div>"+

                            "</div>"
                            +
                            cursos 
                        );

                        $('#dialog_good').dialog({
                            width: "80%",

                        });
                    } else {
                        $(".dialog_text").text("El usuario con cédula: " + cedula + ". No ha realizado ningún curso");
                        $("#dialog_bad").dialog();
                    }


                });




return false;
        }
        
    }
    $('#search_input').keypress(search_function);
    $('#search_form').submit(search_function);




});