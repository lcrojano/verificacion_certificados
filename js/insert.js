
$(document).ready(function () {
  var myDataRefRoot = new Firebase('https://certificado.firebaseio.com');
  var myDataRefRootCursos = new Firebase('https://certificado.firebaseio.com/cursos');
  var myDataRefCertficados = new Firebase('https://certificado.firebaseio.com/certificados');

 $( "#export" ).on( "click", function() {
  $('#certificados_list').tableExport({type:'excel',escape:'false'});
});


  function selectFunction(ct,$i){
	  	if(ValidateDate($('#datetimepickerIni').val()) && ValidateDate($('#datetimepickerFin').val())){
			  $("#certificados_list tbody tr").remove();
			  myDataRefCertficados
			  .orderByChild("fecha_ini")
			  .startAt($('#datetimepickerIni').val())
			  .endAt($('#datetimepickerFin').val())
			  .on("child_added", function(snapshot) {
			  	console.log(snapshot.key() + " was " + snapshot.val().fecha_ini + "");
			  	
			  	$("#certificados_list > tbody:last").append(
			  		"<tr>"+
				  		"<td>"+snapshot.val().cedula+"</td>" +
				        "<td>"+snapshot.val().nombres+"</td>" +
				       	"<td>"+snapshot.val().apellidos+"</td>" +
				       	"<td>"+snapshot.val().certificado+"</td>" + 
				       	"<td>"+snapshot.key()+"</td>" +
				        "<td>"+snapshot.val().fecha_ini+"</td>" +
				        "<td>"+snapshot.val().fecha_fin+"</td>"+
			        "</tr>"
			  		);

			});
		}

	  }
	  function ValidateDate(dtValue)
	  {
	  	var dtRegex = new RegExp(/\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/);
	  	return dtRegex.test(dtValue);
	  }
	   function combo(){
	   	myDataRefRootCursos.on("child_added", function(snapshot) {
		  var curso = snapshot.val();
		  console.log(curso);
		  $("#combo_cursos").append(
			  		"<option>"+ curso + "</option>"
			  		);

		});
}
combo();



  $('#datetimepicker').datetimepicker(
  	{format:'Y/m/d',
  	timepicker:false
  	});
  $('#datetimepicker2').datetimepicker({
  	format:'Y/m/d',
  	timepicker:false
  });


 $('#datetimepickerIni').datetimepicker({
 	  format:'Y/m/d',
  onShow:function( ct ){
   this.setOptions({
    maxDate:$('#datetimepickerFin').val()?$('#datetimepickerFin').val():false
   })
  },
  timepicker:false,
  onSelectDate:selectFunction
 });


 $('#datetimepickerFin').datetimepicker({
 	  format:'Y/m/d',
  onShow:function( ct ){
   this.setOptions({
    minDate:$('#datetimepickerIni').val()?$('#datetimepickerIni').val():false
   })
  },
  timepicker:false,
  onSelectDate:selectFunction
 });






$( "#insert_submit" ).on( "click", function() {
	 var nombres = $('#nombres_input').val();
     var apellidos = $('#apellidos_input').val();
     var cedula = $('#cedula_input').val();
     var certificado = $('#combo_cursos').val();
     var codigo_certificado = $('#codigo_input').val();
     var fecha_ini = $('#datetimepicker').val();
     var fecha_fin = $('#datetimepicker2').val();

var usersRef = myDataRefRoot.child("usuarios");
usersRef.child(cedula).set({
   nombres: nombres,
   apellidos: apellidos,
   cedula: cedula
  }
);
var certificadosRef = myDataRefRoot.child("certificados");
certificadosRef.child(codigo_certificado).set({
	    nombres: nombres,
        apellidos: apellidos,
        cedula: cedula,
   	    certificado: certificado,
   	    codigo_certificado: codigo_certificado,
        fecha_ini: fecha_ini,
        fecha_fin: fecha_fin
  }
);
     

     $('#nombres_input').val('');
     $('#apellidos_input').val('');
     $('#cedula_input').val('');

     $('#codigo_input').val('');
          $('#datetimepicker2').val('');
               $('#datetimepicker').val('');


     return false;
});


myDataRefCertficados.on('child_added', function(snapshot) {
  var cert = snapshot.val();
  //$.each(certificado, function(index, cert){

  displayCertificados(cert.codigo_certificado, cert.certificado, cert.cedula, cert.nombres, cert.apellidos, cert.fecha_ini, cert.fecha_fin);

  //});
});
myDataRefCertficados.on('child_changed', function(snapshot) {
  var cert = snapshot.val();
  //$.each(certificado, function(index, cert){
  changeCertificados(cert.codigo_certificado, cert.certificado, cert.cedula, cert.nombres, cert.apellidos, cert.fecha_ini, cert.fecha_fin);

  //});
});

   function displayCertificados(codigo, certificado, cedula, nombres, apellidos, inicio, fin) {
       $('<tr/>').html(
        "<td>"+cedula+"</td>" +
        "<td>"+nombres+"</td>" +
       	"<td>"+apellidos+"</td>" +
       	"<td>"+certificado+"</td>" + 
       	"<td>"+codigo+"</td>" +
        "<td>"+inicio+"</td>" +
        "<td>"+fin+"</td>"
       

       	).appendTo($('#certificados_list'));
        $('#certificados_list')[0].scrollTop = $('#certificados_list')[0].scrollHeight;
      };
      var ok = false;
      function changeCertificados(codigo, certificado, cedula, nombres, apellidos, inicio, fin) {

         	$('#certificados_list tbody tr').each(
         	function(i) 
         	{
         		
         		var found = $(this).find("td").each(
         			 function(j){
		         			if($.trim($(this).text()).toLowerCase() === $.trim(codigo).toLowerCase()){
		         				ok = true;
		         				return	false;
		         			}
	         	});

         		
         		

         		if(ok) {
         			$('#certificados_list tbody tr').eq(i).html(
					        "<td>"+cedula+' '+"</td>" +
					        "<td>"+nombres+' '+"</td>" +
					       	"<td>"+apellidos+' '+"</td>" +
					       	"<td>"+certificado+"</td>" + 
					       	"<td>"+codigo+' '+"</td>" +
                        "<td>"+inicio+' '+"</td>" +
                              "<td>"+fin+' '+"</td>"
					       	);
         			ok = false
					       return false;
				}
				


         	});
         }

});

